package edu.westga.movietheater.model;

/**
 * Represents one showing of a movie
 * 
 * @author lewisb
 *
 */
public class Showing {

	private String movie;
	private int ticketsSold;
	private double admission;
	
	/**
	 * Creates a new showing of the given movie with the given ticket price
	 * 
	 * @param movie the movie's name
	 * @param admission the price of a ticket
	 */
	public Showing(String movie, double admission) {
		this.movie = movie;
		this.admission = admission;
		this.ticketsSold = 0;
	}
	
	/**
	 * Gets the name of the movie associated with this showing.
	 * 
	 * @return the movie's name
	 */
	public String getMovieName() {
		return this.movie;
	}
	
	/**
	 * Gets the price of a ticket.
	 * 
	 * @return the price of a ticket
	 */
	public double getTicketPrice() {
		return this.admission;
	}
	
	/**
	 * Gets the number of tickets sold.
	 * 
	 * @return the number of tickets sold.
	 */
	public int getNumberOfTicketsSold() {
		return this.ticketsSold;
	}
	
	/**
	 * Sells a single ticket.
	 */
	public void sellTicket() {
		this.ticketsSold += 1;
	}
	
	/**
	 * Returns the amount of money we've made on this showing.
	 * 
	 * @return the total, in ticket sales, for this showing
	 */
	public double getGrossSales() {
		return this.getNumberOfTicketsSold() * this.admission;
	}
}
