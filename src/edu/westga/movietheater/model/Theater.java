package edu.westga.movietheater.model;

import java.util.ArrayList;

/**
 * A theater is a collection of showings.
 * 
 * @author lewisb
 *
 */
public class Theater {

	private ArrayList<Showing> shows;
	
	/**
	 * Creates a new Theater with no shows.
	 */
	public Theater() {
		this.shows = new ArrayList<Showing>();
	}
	
	/**
	 * Adds a showing to this theater
	 * 
	 * @param movie the showing to add
	 */
	public void add(Showing movie) {
		this.shows.add(movie);
	}
	
	/**
	 * Gets all the showings in this theater -- as an array.
	 * 
	 * @return an array of all the showings in this theater
	 */
	public Showing[] getShows() {
		return this.shows.toArray(new Showing[0]);
	}
	
	/**
	 * Gets the total amount of money made on all shows at this theater.
	 * 
	 * @return the total sales.
	 */
	public double getTotalSales() {
		double sum = 0.0;
		for (Showing show: this.shows) {
			sum += show.getGrossSales();
		}
		return sum;
	}
	
	/**
	 * Gets the total number of tickets sold across all shows at this theater.
	 * 
	 * @return the total number of tickets sold
	 */
	public int getTotalTicketsSold() {
		int sum = 0;
		for (Showing show: this.shows) {
			sum += show.getNumberOfTicketsSold();
		}
		return sum;
	}
}
