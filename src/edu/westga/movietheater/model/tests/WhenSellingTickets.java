package edu.westga.movietheater.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.movietheater.model.Showing;

/**
 * Tests that the number of tickets sold are properly tracked and that the dollar amounts
 * of those tickets sold are properly calculated.
 * 
 * @author lewisb
 *
 */
public class WhenSellingTickets {

	private Showing show;
	
	@Before
	public void setUp() {
		this.show = new Showing("Star Wars", 15.75);
	}
	
	/**
	 * Tests that our gross sales are $0.00 if no tickets have been sold.
	 */
	@Test
	public void noTicketsSoldMeansNoGrossSales() {
		assertEquals(0.0, this.show.getGrossSales(), 0.001);
	}
	
	/**
	 * Tests the case where one ticket has been sold.
	 */
	@Test
	public void shouldSellOneTicket() {
		this.show.sellTicket();
		assertEquals(1, this.show.getNumberOfTicketsSold());
		assertEquals(15.75, this.show.getGrossSales(), 0.001);
	}

	/**
	 * Tests the case where several tickets have been sold.
	 */
	@Test
	public void shouldSellManyTickets() {
		this.show.sellTicket();
		this.show.sellTicket();
		this.show.sellTicket();
		this.show.sellTicket();
		this.show.sellTicket();
		assertEquals(5, this.show.getNumberOfTicketsSold());
		assertEquals(78.75, this.show.getGrossSales(), 0.001);
	}
}
