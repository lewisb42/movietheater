package edu.westga.movietheater.model.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.movietheater.model.Showing;

/**
 * Tests for the constructor of the Showing class
 * @author lewisb
 *
 */
public class WhenCreatingShowings {

	/**
	 * Tests that the constructor parameters properly initialize
	 * a Showing object
	 */
	@Test
	public void shouldCreateShowing() {
		Showing show = new Showing("Star Wars", 12.50);
		assertEquals("Star Wars", show.getMovieName());
		assertEquals(12.5, show.getTicketPrice(), 0.001);
		assertEquals(0, show.getNumberOfTicketsSold());
	}

}
