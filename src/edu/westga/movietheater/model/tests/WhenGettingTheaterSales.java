package edu.westga.movietheater.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.movietheater.model.Showing;
import edu.westga.movietheater.model.Theater;

/**
 * Tests for the getTotalSales() method of Theater
 * 
 * @author lewisb
 *
 */
public class WhenGettingTheaterSales {

	private Theater theater;
	
	private Showing anh;
	private Showing esb;
	private Showing rotj;
	
	@Before
	public void setUp() {
		this.theater = new Theater();
		this.anh = new Showing("A New Hope", 10.00);
		this.anh.sellTicket();
		this.anh.sellTicket();
		this.anh.sellTicket();
		this.esb = new Showing("Empire Strikes Back", 15.75);
		this.esb.sellTicket();
		this.rotj = new Showing("Return of the Jedi", 9.99);
		this.rotj.sellTicket();
		this.rotj.sellTicket();
	}
	
	/**
	 * Case where there are no sales at the theater.
	 */
	@Test
	public void shouldNotHaveAnySales() {
		assertEquals(0.0, this.theater.getTotalSales(), 0.001);
	}
	
	/**
	 * Case where there is only one showing at this theater.
	 */
	@Test
	public void shouldProperlySumSalesFromOneShowing() {
		this.theater.add(this.anh);
		assertEquals(30.00, this.theater.getTotalSales(), 0.001);
	}
	
	/**
	 * Case where there are several showings at this theater.
	 */
	@Test
	public void shouldProperlySumSalesFromSeveralShowings() {
		this.theater.add(this.anh);
		this.theater.add(this.esb);
		this.theater.add(this.rotj);
		assertEquals(65.73, this.theater.getTotalSales(), 0.001);
	}

}
