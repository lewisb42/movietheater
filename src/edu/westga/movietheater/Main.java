package edu.westga.movietheater;

import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.movietheater.model.Showing;

/**
 * Main class for the MovieTheater project
 * 
 * @author lewisb
 *
 */
public class Main {

	/**
	 * Starting point for the program
	 * 
	 * @param args n/a
	 */
	public static void main(String[] args) {
		ArrayList<Showing> movies = new ArrayList<Showing>();

		Showing starWars = new Showing("1. Star Wars", 15.00);
		Showing alvin = new Showing("2. Alvin and the Chipmunks", 1.75);
		Showing theMatrix = new Showing("3. The Matrix", 12.25);
		
		movies.add(starWars);
		movies.add(alvin);
		movies.add(theMatrix);
		
		System.out.println("Showing today:");
		System.out.println("--------------");
		for (Showing show: movies) {
			System.out.println(show.getMovieName());
		}
		
		Scanner kb = new Scanner(System.in);
		while (true) {
			System.out.println("Buy ticket for:");
			String input = kb.nextLine();
			if (input.equals("done")) {
				break;
			} else if (input.equals("1")) {
				starWars.sellTicket();
			} else if (input.equals("2")) {
				alvin.sellTicket();
			} else if (input.equals("3")) {
				theMatrix.sellTicket();
			}
			
			System.out.println("Sales today");
			System.out.println("-----------");
			String header = String.format("%-30s   %4s   %6s", "Movie", "#tix", "total$");
			System.out.println(header);
			for (Showing show: movies) {
				String line = String.format("%-30s   %4d   %3.2f", show.getMovieName(), show.getNumberOfTicketsSold(), show.getGrossSales());
				System.out.println(line);
			}
		}
	}

}
